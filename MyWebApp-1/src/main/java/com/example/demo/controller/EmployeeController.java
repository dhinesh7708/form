package com.example.demo.controller;

import java.io.IOException;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.EmployeeService;
import com.example.demo.vo.EmployeeVO;
import com.example.demo.vo.PaginationVO;
import com.example.demo.vo.ResponseVO;
import com.example.demo.vo.TableData;

@RestController
@CrossOrigin
public class EmployeeController {
	@Autowired
	EmployeeService service;

	@PostMapping("/save")
	public ResponseVO save(@RequestBody EmployeeVO employee) {
		return service.save(employee);
	}

	@GetMapping("/get/{id}")
	public EmployeeVO getEmployee(@PathVariable(name = "id") String id) {
		return service.getEmployeeInfo(UUID.fromString(id));
	}

	@PostMapping("/get/employee/list")
	public TableData getEmployeeList(@RequestBody PaginationVO paginationVO) throws IOException {
		return service.getEmployeeList(paginationVO);
	}

	@GetMapping("/get/delete/employee/detail/{id}")
	public ResponseVO deleteEmployeeInfo(@PathVariable(name = "id") String id) {
		return service.deleteEmployeeInfo(UUID.fromString(id));
	}

}
