package com.example.demo.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entities.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, UUID> {

	@Query("select e from Employee e where e.activeFlag='Y'")
	public List<Employee> getEmployeeList(Pageable pageable);

	@Query("select count(e) from Employee e where e.activeFlag='Y'")
	public String getTotalCountForGrid();
}
