package com.example.demo.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Employee;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.vo.EmployeeVO;
import com.example.demo.vo.PaginationVO;
import com.example.demo.vo.ResponseVO;
import com.example.demo.vo.TableData;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;

	private Employee construcVOToDTO(EmployeeVO employeeVO) {
		return Employee.builder().name(employeeVO.getName()).gender(employeeVO.getGender())
				.address(employeeVO.getAddress()).activeFlag("Y").build();
	}

	private EmployeeVO construcVOToDTO(Employee employee) {
		return EmployeeVO.builder().id(employee.getId()).name(employee.getName()).gender(employee.getGender())
				.address(employee.getAddress()).build();
	}

	public ResponseVO save(EmployeeVO employeeVo) {
		if (employeeVo.getId() == null) {
			Employee employee = construcVOToDTO(employeeVo);
			employeeRepository.save(employee);
			return ResponseVO.builder().response("saved").build();
		} else {
			Employee employee = employeeRepository.getOne(employeeVo.getId());
			employee.setAddress(employeeVo.getAddress());
			employee.setName(employeeVo.getName());
			employee.setGender(employeeVo.getGender());
			employeeRepository.save(employee);
			return ResponseVO.builder().response("updated").build();
		}
	}

	public EmployeeVO getEmployeeInfo(UUID id) {
		Employee employee = employeeRepository.getOne(id);
		return construcVOToDTO(employee);
	}

	public ResponseVO deleteEmployeeInfo(UUID id) {
		Employee employee = employeeRepository.getOne(id);
		employee.setActiveFlag("N");
		employeeRepository.save(employee);
		return ResponseVO.builder().response("Deleted Successfully").build();
	}

	protected Pageable getPageableObject(PaginationVO vo) {
		Sort sort = null;
		int pageSize = 10;

		if (vo.getSize() > 0) {
			pageSize = vo.getSize();
		}
		if (StringUtils.equals(vo.getDirection(), "asc")) {
			sort = Sort.by(new Sort.Order(Direction.ASC, vo.getColumnName()));
		} else if (StringUtils.equals(vo.getDirection(), "desc")) {
			sort = Sort.by(new Sort.Order(Direction.DESC, vo.getColumnName()));
		}
		if (sort != null) {
			return PageRequest.of(vo.getIndex(), pageSize, sort);
		}

		return PageRequest.of(vo.getIndex(), pageSize);
	}

	@Transactional
	public TableData getEmployeeList(PaginationVO pagination) throws IOException {
		List<Map<String, String>> list = new ArrayList<>();
		Map<String, String> dataMap = null;
		Pageable pageable = getPageableObject(pagination);
		String totalCount = employeeRepository.getTotalCountForGrid();
		List<Employee> employeeList = employeeRepository.getEmployeeList(pageable);
		for (Employee employee : employeeList) {
			dataMap = new HashMap<>();
			dataMap.put("id", employee.getId().toString());
			dataMap.put("name", employee.getName());
			dataMap.put("gender", employee.getGender());
			dataMap.put("address", employee.getAddress());
			list.add(dataMap);
		}
		return TableData.builder().data(list).totalRecords(totalCount).build();
	}
}