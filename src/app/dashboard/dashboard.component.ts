import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  
  
  constructor(private fb: FormBuilder, private router:Router
    ) { }

  ngOnInit(): void {
  }

  getForm(){
    this.router.navigate(['/employee-form']);
  }
  getList(){
    this.router.navigate(['/employee-list']);
}
}
