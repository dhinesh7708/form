import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Employee } from '../heroes/employee-vo';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-employee-dialog',
  templateUrl: './employee-dialog.component.html',
  styleUrls: ['./employee-dialog.component.css']
})
export class EmployeeDialogComponent implements OnInit{

  constructor(private fb: FormBuilder,public dialogRef: MatDialogRef<EmployeeDialogComponent>,public dialog:MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) { }
    employeeForm: FormGroup;
    ngOnInit() {
      console.log(this.data);
      this.employeeForm = this.fb.group({
        id:[this.data.id],
        name: [this.data.name, [Validators.required]],
        gender: [this.data.gender, [Validators.required]],
        address: [this.data.address, [Validators.required]]
  
      })
    }
    setForm() {
      this.dialogRef.close(this.employeeForm.value);
    }

    onClose(){
      this.dialogRef.close();
    }
}
