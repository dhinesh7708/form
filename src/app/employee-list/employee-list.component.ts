import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EmployeeService } from '../heroes/employee.service';
import { PaginationVO } from '../heroes/pagination-vo';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { merge } from 'rxjs';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
 
  pagination = new PaginationVO();

  length: any
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10];

  constructor( private fb: FormBuilder,private router:Router, private employeeService: EmployeeService) { }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = ['id', 'name', 'gender', 'address'];
  dataSource: any;
  employeeForm: FormGroup;
  ngOnInit(): void {
    this.getList();
  }
getList() {
  this.pagination.columnName = "name";
  this.pagination.direction = "asc";
  if (this.paginator.pageSize) {
    this.pagination.size = this.paginator.pageSize;
  } else {
    this.pagination.size = this.pageSize;
  }
  this.pagination.index = this.paginator.pageIndex;

  this.employeeService.getEmployeeList(this.pagination).subscribe(data => {
    console.log(data);
    this.dataSource = data.data;
    this.length = data.totalRecords;
  })
}

ngAfterViewInit() {
  merge(this.paginator.page).subscribe(() => this.getList());
}

}
