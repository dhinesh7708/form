export class Employee {
  id: string;
  name: string;
  gender: string;
  address: string;
}