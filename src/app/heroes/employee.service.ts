import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  saveEmployeeUrl = environment.baseurl + '/emp-details/save';
  getEmployeeurl = environment.baseurl + '/emp-details/get/';
  getListurl = environment.baseurl + '/emp-details/get/employee/list';
  deleteDetailurl = environment.baseurl + '/emp-details/get/delete/employee/detail/';
  saveEmployee(employeeVo) {
    return this.http.post<any>(this.saveEmployeeUrl, employeeVo);
  }
  getEmployee(id) {
    return this.http.get<any>(this.getEmployeeurl + id);
  }

  getEmployeeList(paginationVO) {
    return this.http.post<any>(this.getListurl, paginationVO);
  }

  deleteEmployeeDetail(id) {
    return this.http.get<any>(this.deleteDetailurl + id);
  }
}
