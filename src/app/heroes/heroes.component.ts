import { Component, OnInit, ViewChild } from '@angular/core';
import { Employee } from './employee-vo';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from './employee.service';
import { PaginationVO } from './pagination-vo';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { merge } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { SnackbarComponent } from '../snackbar/snackbar.component';
import { MatDialog } from '@angular/material/dialog';
import { EmployeeDialogComponent } from '../employee-dialog/employee-dialog.component';



@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})

export class HeroesComponent implements OnInit {
  length: any
  pageSize = 5;
  pageSizeOptions: number[] = [5, 10];

  employeeForm: FormGroup;
  employeeVO: Employee;
  name: Employee;
  popup: any;
  pagination = new PaginationVO();
  pageEvent: PageEvent;
  employeeDialog: EmployeeDialogComponent;
  
  constructor(private fb: FormBuilder, private employeeService: EmployeeService, private snackbar: MatSnackBar, private dialog: MatDialog) { }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = ['id', 'name', 'gender', 'address'];
  dataSource: any;
  ngOnInit() {
    this.employeeForm = this.fb.group({
      id: [],
      name: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      address: ['', [Validators.required]]

    })
    this.getList();
  }

  save(userForm) {
    this.employeeVO = this.employeeForm.getRawValue();
    console.log(this.employeeVO);
    if (userForm.valid) {
      this.employeeService.saveEmployee(this.employeeVO).subscribe(data => {
        console.log(data);
        this.snackbar.openFromComponent(SnackbarComponent, {
          data: data.response,
        });
        userForm.resetForm();
        this.refreshGrid();
      })
    }
  }

  deleteId(userForm) {
    this.employeeVO = this.employeeForm.getRawValue();
    if (userForm.valid) {
      this.employeeService.deleteEmployeeDetail(this.employeeVO.id).subscribe(data => {
        console.log(data);
        this.snackbar.openFromComponent(SnackbarComponent, {
          data: data.response,
        });
        userForm.resetForm();
        this.refreshGrid();
      });
    }
  }

  getId(id) {
    this.employeeService.getEmployee(id).subscribe(data => {
      console.log(data);
      const dialogRef = this.dialog.open(EmployeeDialogComponent, {
        width: '800px',
        height: '400px',
        data: data
      });
      dialogRef.afterClosed().subscribe(value => {
        console.log(value);
        if(value){
          this.employeeForm.setValue(value)
        }
      })
    })
  }

  getList() {
    this.pagination.columnName = "name";
    this.pagination.direction = "asc";
    if (this.paginator.pageSize) {
      this.pagination.size = this.paginator.pageSize;
    } else {
      this.pagination.size = this.pageSize;
    }
    this.pagination.index = this.paginator.pageIndex;

    this.employeeService.getEmployeeList(this.pagination).subscribe(data => {
      console.log(data);
      this.dataSource = data.data;
      this.length = data.totalRecords;
    })
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
  }

  ngAfterViewInit() {
    merge(this.paginator.page).subscribe(() => this.getList());
  }

  refreshGrid() {
    this.getList();
  }
  setForm(id){
    this.employeeService.getEmployee(id).subscribe(data => {
      console.log(data);
      this.employeeForm.setValue(data)
    });
  }

    
}
