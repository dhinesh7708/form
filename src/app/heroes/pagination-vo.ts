export class PaginationVO {
    index: number;
    size: number;
    direction: string;
    columnName: string;
}